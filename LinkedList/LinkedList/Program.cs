﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            MyLinkedList<int> list = new MyLinkedList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            
            foreach (var item in list)
            {
                
                Console.WriteLine(item);
            }

            list.RemoveElement(5);
            list.RemoveElement(1);
            list.RemoveElement(8);
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            Console.WriteLine(list.Count);
            Console.ReadLine();
        }
    }
}
