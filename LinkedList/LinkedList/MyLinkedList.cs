﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    public class MyLinkedList<T> : IEnumerable
    {
        
        public Item<T> FirstElement { get; private set; }
        public Item<T> LastElement { get; private set; }


        public int Count { get; private set; }

        public MyLinkedList(T data)
        {
            var item = new Item<T>(data);
            FirstElement = item;
            LastElement = item;
            Count++;
        }
        public MyLinkedList()
        {
            FirstElement = null;
            LastElement = null;
            Count = 0;
        }
        public void Add(T data)
        {
            var item = new Item<T>(data);
            if (FirstElement == null)
            {
                FirstElement = item;
                LastElement = item;
                Count++;
            }
            else
            {
                LastElement.Next = item;
                LastElement = item;
                Count++;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        public void RemoveElement(T data)
        {
            var current = FirstElement.Next;
            var previous = FirstElement;

            if (FirstElement.Data.Equals(data))
            {
                FirstElement = FirstElement.Next;
                Count--;
                return;
            }
            
            while (current!=null)
            {
                if (current.Data.Equals(data))
                {
                    previous.Next = current.Next;
                    Count--;
                    if (LastElement.Data.Equals(data))
                        LastElement = previous;
                    return;
                }
                current = current.Next;
                previous = previous.Next;
            }
            
        }
        private class Enumerator : IEnumerator
        {
            private Item<T> _item;
            private MyLinkedList<T> _list;
            public Enumerator(MyLinkedList<T> list)
            {
                _list = list;
                _item = list.FirstElement;

            }
            public object Current { get; private set; }
            public bool MoveNext()
            {
                if (_item == null)
                    return false;
                Current = _item.Data;
                _item = _item.Next;
                return true;
            }

            public void Reset()
            {
                _list = null; 
            }
        }
    }
}
