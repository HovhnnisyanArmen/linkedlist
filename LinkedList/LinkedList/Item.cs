﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    public class Item<T>
    {
        public Item(T data)
        {
            Data = data;
        }
        private T data;
        public T Data
        {
            get => data;
            set
            {
                if (value != null)
                    data = value;
                else
                    data = default(T);
            }
        }
        public override string ToString()
        {
            return data.ToString();
        }
        public  Item<T> Next { get; set; }

    }
}
